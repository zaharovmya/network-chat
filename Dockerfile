FROM golang:1.14-alpine AS builder

RUN mkdir -p /api
WORKDIR /api

COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build -o ./app-chat ./cmd

EXPOSE 5050
ENTRYPOINT ["./app-chat"]