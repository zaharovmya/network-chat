package app

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type DBConnectionParams struct {
	UserName string
	Password string
	URI      string
	Port     string
	DBName   string
}

func NewDBConn(dbParams *DBConnectionParams) *sqlx.DB {
	connString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbParams.UserName, dbParams.Password, dbParams.URI, dbParams.Port, dbParams.DBName)
	conn, err := sqlx.Connect("mysql", connString)
	if err != nil {
		panic(err)
	}
	return conn
}
