package app

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"gitlab.com/zaharovmya/network-chat/app/repository"
	api "gitlab.com/zaharovmya/network-chat/client/chat"
)

type ChatService struct {
	api.UnimplementedChatServiceServer
	repository *repository.MessageRepository
}

func NewChatService(repository *repository.MessageRepository) *ChatService {
	return &ChatService{repository: repository}
}


func (cs *ChatService) AddMessage(ctx context.Context, request *api.AddMessageRequest) (*api.AddMessageResponse, error) {

	span, ctx := opentracing.StartSpanFromContext(ctx, "AddMessage")
	defer span.Finish()
	if err := cs.repository.Create(request.ChatId, request.UserId, request.Message); err != nil {
		return nil, err
	}
	return &api.AddMessageResponse{Code: 1}, nil
}

func (cs *ChatService) GetMessages(ctx context.Context, request *api.GetMessagesRequest) (*api.GetMessagesResponse, error) {

	span, ctx := opentracing.StartSpanFromContext(ctx, "GetMessages")
	defer span.Finish()

	response, err := cs.repository.List(request.ChatId)

	if err != nil {
		return nil, err
	}

	messages := make([]*api.Message, len(response))

	for i, msg := range response {
		messages[i] = &api.Message{ChatId: msg.ChatID, Id: msg.ID, UserId: msg.UserID, Message: msg.Message}
	}

	return &api.GetMessagesResponse{Messages: messages}, nil
}
