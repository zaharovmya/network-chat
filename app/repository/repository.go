package repository

import "github.com/jmoiron/sqlx"

type Message struct {
	ID      string `db:"id"`
	ChatID  string `db:"chat_id"`
	UserID  int64  `db:"user_id"`
	Message string `db:"message"`
}

type Repository interface {
	List(string) ([]Message, error)
	Create(string, int64, string) error
}


type MessageRepository struct {
	masterDBConn *sqlx.DB
	slaveDBConn *sqlx.DB
}

var _ Repository = (*MessageRepository)(nil)

func NewMessageRepository(masterDBConn, slaveDBConn *sqlx.DB) *MessageRepository {
	return &MessageRepository{
		masterDBConn: masterDBConn,
		slaveDBConn: slaveDBConn,
	}
}

func (r *MessageRepository) List(chatID string) ([]Message, error) {
	var messages []Message

	query := "SELECT id, chat_id, user_id, message FROM messages WHERE chat_id=? ORDER BY created_at"
	if err := r.slaveDBConn.Select(&messages, query, chatID); err != nil {
		return nil, err
	}
	return messages, nil
}

func (r *MessageRepository) Create(chatID string, userID int64, message string) error {
	var query = "INSERT INTO messages (`chat_id`, `user_id`, `message`) VALUES (?, ?, ?)"
	if _, err := r.masterDBConn.Exec(query, chatID, userID, message); err != nil {
		return err
	}
	return nil
}