package main

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/zaharovmya/network-chat/app/repository"
	"io"
	"log"
	"net"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/zaharovmya/network-chat/app"
	pb "gitlab.com/zaharovmya/network-chat/client/chat"
	"google.golang.org/grpc"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-lib/metrics"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/tracing"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	jaegerlog "github.com/uber/jaeger-client-go/log"
)

func newTracer() (opentracing.Tracer, io.Closer, error) {
	cfg := jaegercfg.Configuration{
		ServiceName: "otus-network-chat",
		Sampler: &jaegercfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans: true,
		},
	}

	jLogger := jaegerlog.StdLogger
	jMetricsFactory := metrics.NullFactory

	tracer, closer, err := cfg.NewTracer(
		jaegercfg.Logger(jLogger),
		jaegercfg.Metrics(jMetricsFactory),
	)

	return tracer, closer, err
}

func main() {
	godotenv.Overload(".env")

	tracer, closer, err := newTracer()
	if err != nil {
		log.Fatalf("Failed to create tracer: %v", err)
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)
	lis, err := net.Listen("tcp", os.Getenv("PORT"))
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	server := grpc.NewServer(
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_opentracing.StreamServerInterceptor(grpc_opentracing.WithTracer(tracer)),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_opentracing.UnaryServerInterceptor(grpc_opentracing.WithTracer(tracer)),
		)),
	)
	messageRepository := repository.NewMessageRepository(newMasterDBConn(), newSlaveDBConn())
	service := app.NewChatService(messageRepository)
	pb.RegisterChatServiceServer(server, service)

	if err := server.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}


func newMasterDBConn() *sqlx.DB {
	dbParams := &app.DBConnectionParams{
		UserName: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
		URI:      os.Getenv("DB_URI"),
		Port:     os.Getenv("DB_PORT"),
		DBName:   os.Getenv("DB_NAME"),
	}
	return app.NewDBConn(dbParams)
}

func newSlaveDBConn() *sqlx.DB {
	dbParams := &app.DBConnectionParams{
		UserName: os.Getenv("SLAVE_DB_USERNAME"),
		Password: os.Getenv("SLAVE_DB_PASSWORD"),
		URI:      os.Getenv("SLAVE_DB_URI"),
		Port:     os.Getenv("SLAVE_DB_PORT"),
		DBName:   os.Getenv("SLAVE_DB_NAME"),
	}
	return app.NewDBConn(dbParams)
}

